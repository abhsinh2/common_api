## Read Excel file as excel defined model

1.Read excel workbook (all the sheets)
 
```java
ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
WorkbookDetails workbook = excelFileReader.readWorkbook(true, false);
```

2.Read excel sheet by name
 
```java
ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
SheetDetails sheet = excelFileReader.readSheet("SHEET_NAME", true, false);
```

3.Read excel sheet by index
 
```java
ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
SheetDetails sheet = excelFileReader.readSheet(0, true, false);
```

## Read Excel file as custom model

### Annotate model class

use `@ExcelDataModel` to specify if excel has header and data to read by row or column
use `@ExcelDataElement` to specify excel column name or index

```java
@ExcelDataModel(columnWise = false, withHeader = true)
public class Person {
	@ExcelDataElement(column = "Name")
	private String name;

	@ExcelDataElement(column = "Age")
	private int age;
	
	... include getter/setter
}
```

```java
@ExcelDataModel(columnWise = true, withHeader = true)
public class Person {
	@ExcelDataElement(column = "Name")
	private List<String> names;

	@ExcelDataElement(column = "Age")
	private List<Integer> ages;
	
	... include getter/setter
}
```

More example can be found [here](./src/test/java/com/pds/dashboard/data/file/excel/data)

1.Read excel workbook (all the sheets)
 
```java
Map<String, Class<?>> map = new HashMap<>();
map.put("sheet1", Person.class);
map.put("sheet2", Address.class);

ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
Map<String, List<?>> workbook = excelFileReader.readWorkbook(map);

List<Person> ls1 = (List<Person>) result.get("sheet1");
List<Address> ls2 = (List<Address>) result.get("sheet2");
```

2.Read excel sheet by name
 
```java
ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
Person person = excelFileReader.readSheet("sheet1", Person.class);
```

3.Read excel sheet by index
 
```java
ExcelFileReader excelFileReader = new ExcelFileReader(new File("<PATH_TO_EXCEL_FILE>"));
Person person = excelFileReader.readSheet(1, Person.class);
```