package com.pds.dashboard.data.file.excel.data;

import com.pds.dashboard.data.file.excel.annotations.ExcelDataElement;

public class PersonWithoutClassTypeAnnotation {
	@ExcelDataElement(column = "Name")
	private String name;

	@ExcelDataElement(column = "Age")
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
}
