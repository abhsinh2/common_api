package com.pds.dashboard.data.file.excel.data;

import java.util.List;

import com.pds.dashboard.data.file.excel.annotations.ExcelDataElement;
import com.pds.dashboard.data.file.excel.annotations.ExcelDataModel;

@ExcelDataModel(columnWise = true, withHeader = false)
public class PersonWithoutHeaderColwise {
	@ExcelDataElement(index = 0)
	private List<String> names;

	@ExcelDataElement(index = 1)
	private List<Integer> ages;

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public List<Integer> getAges() {
		return ages;
	}

	public void setAges(List<Integer> ages) {
		this.ages = ages;
	}

	@Override
	public String toString() {
		return "Person [names=" + names + ", ages=" + ages + "]";
	}
}
