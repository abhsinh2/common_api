package com.pds.dashboard.data.file.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelFileReaderTestHelper {

	public static final String SHEET_WITHOUT_HEADER = "Sheet1";
	public static final String SHEET_WITH_HEADER_WITH_DIFF_NAME = "Sheet2";
	public static final String SHEET_WITH_HEADER_WITH_SAME_NAME = "Sheet3";
	public static final String SHEET_WITH_HEADER_WITH_DIFF_NAME_2 = "Sheet4";

	public void writeExcel(File fileLocation) throws IOException {
		Workbook workbook = new XSSFWorkbook();

		try {
			Sheet sheetWithoutHeader = this.createSheet(SHEET_WITHOUT_HEADER, workbook);
			Sheet sheetWithHeaderWithDiffName = this.createSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME, workbook);
			Sheet sheetWithHeaderWithSameName = this.createSheet(SHEET_WITH_HEADER_WITH_SAME_NAME, workbook);
			Sheet sheetWithHeaderWithDiffName_2 = this.createSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME_2, workbook);
			
			// sheetWithoutHeader
			int index = 0;
			this.createRow("Ram", 10, sheetWithoutHeader, index++);
			this.createRow("Shyam", 20, sheetWithoutHeader, index++);
			
			// sheetWithHeaderWithDiffName
			index = 0;
			Row header = sheetWithHeaderWithDiffName.createRow(index++);
			header.createCell(0).setCellValue("Name");
			header.createCell(1).setCellValue("Age");
			this.createRow("Ram", 10, sheetWithHeaderWithDiffName, index++);
			this.createRow("Shyam", 20, sheetWithHeaderWithDiffName, index++);
			
			// sheetWithHeaderWithSameName
			index = 0;
			header = sheetWithHeaderWithSameName.createRow(index++);
			header.createCell(0).setCellValue("name");
			header.createCell(1).setCellValue("age");
			this.createRow("Ram", 10, sheetWithHeaderWithSameName, index++);
			this.createRow("Shyam", 20, sheetWithHeaderWithSameName, index++);
			
			// sheetWithHeaderWithDiffName_2
			index = 0;
			header = sheetWithHeaderWithDiffName_2.createRow(index++);
			header.createCell(0).setCellValue("Name");
			header.createCell(1).setCellValue("Age");
			this.createRow("Ram", 10, sheetWithHeaderWithDiffName_2, index++);
			this.createRow("Shyam", 20, sheetWithHeaderWithDiffName_2, index++);

			workbook.write(new FileOutputStream(fileLocation));
		} finally {
			if (workbook != null) {
				workbook.close();
			}
		}
	}

	private Sheet createSheet(String name, Workbook workbook) {
		return workbook.createSheet(name);
	}

	private void createRow(String name, int age, Sheet sheet, int index) {
		Row row = sheet.createRow(index);

		Cell cell = row.createCell(0);
		cell.setCellValue(name);

		cell = row.createCell(1);
		cell.setCellValue(age);
	}
}
