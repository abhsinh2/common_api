package com.pds.dashboard.data.file.excel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.pds.dashboard.data.file.FileReaderException;
import com.pds.dashboard.data.file.InvalidModelException;
import com.pds.dashboard.data.file.excel.data.PersonWithHeaderColwise;
import com.pds.dashboard.data.file.excel.data.PersonWithHeaderRowwise;
import com.pds.dashboard.data.file.excel.data.PersonWithoutAnyAnnotation;
import com.pds.dashboard.data.file.excel.data.PersonWithoutClassTypeAnnotation;
import com.pds.dashboard.data.file.excel.data.PersonWithoutFieldTypeAnnotation;
import com.pds.dashboard.data.file.excel.data.PersonWithoutHeaderColwise;
import com.pds.dashboard.data.file.excel.data.PersonWithoutHeaderRowwise;
import com.pds.dashboard.data.file.excel.model.SheetDetails;
import static com.pds.dashboard.data.file.excel.ExcelFileReaderTestHelper.*;

public class ExcelFileReaderTest {

	private ExcelFileReaderTestHelper excelDataProviderTestHelper;

	private static String EXCEL_FILE = "testFile.xlsx";

	private File excelFile;
	private ExcelFileReader excelFileReader;

	@Before
	public void generateExcelFile() throws IOException {
		File currDir = new File(".");
		String path = currDir.getAbsolutePath();

		String excelFileLocation = path.substring(0, path.length() - 1) + EXCEL_FILE;
		excelFile = new File(excelFileLocation);

		excelDataProviderTestHelper = new ExcelFileReaderTestHelper();
		excelDataProviderTestHelper.writeExcel(excelFile);

		excelFileReader = new ExcelFileReader(excelFile);
	}

	@Test
	public void test_row_withHeader_rowwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME, true, false);

		assertEquals("Name", sheet.getHeader().getColumn(0).getData().getValue());
		assertEquals("Age", sheet.getHeader().getColumn(1).getData().getValue());

		assertEquals("Ram", sheet.getRow(0).getColumn(0).getData().getValue());
		assertEquals("10", sheet.getRow(0).getColumn(1).getData().getValue());
	}

	@Test
	public void test_row_withHeader_colwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME, true, true);

		assertEquals("Name", sheet.getHeader().getColumn(0).getData().getValue());
		assertEquals("Age", sheet.getHeader().getColumn(1).getData().getValue());

		assertEquals("Ram", sheet.getRow(0).getColumn(0).getData().getValue());
		assertEquals("10", sheet.getRow(0).getColumn(1).getData().getValue());
	}

	@Test
	public void test_column_withHeader_rowwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME, true, false);

		assertEquals("Name", sheet.getHeader().getColumn(0).getData().getValue());
		assertEquals("Age", sheet.getHeader().getColumn(1).getData().getValue());

		assertEquals("Ram", sheet.getColumns("Name").get(0).getValue());
		assertEquals("10", sheet.getColumns("Age").get(0).getValue());
	}

	@Test
	public void test_column_withHeader_colwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME, true, true);

		assertEquals("Name", sheet.getHeader().getColumn(0).getData().getValue());
		assertEquals("Age", sheet.getHeader().getColumn(1).getData().getValue());

		assertEquals("Ram", sheet.getColumns("Name").get(0).getValue());
		assertEquals("10", sheet.getColumns("Age").get(0).getValue());
	}

	@Test
	public void test_row_withoutHeader_rowwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITHOUT_HEADER, false, false);

		assertNull(sheet.getHeader());

		assertEquals("Ram", sheet.getRow(0).getColumn(0).getData().getValue());
		assertEquals("10", sheet.getRow(0).getColumn(1).getData().getValue());

		assertEquals("Shyam", sheet.getRow(1).getColumn(0).getData().getValue());
		assertEquals("20", sheet.getRow(1).getColumn(1).getData().getValue());
	}

	@Test
	public void test_row_withoutHeader_colwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITHOUT_HEADER, false, true);

		assertNull(sheet.getHeader());

		assertEquals("Ram", sheet.getRow(0).getColumn(0).getData().getValue());
		assertEquals("10", sheet.getRow(0).getColumn(1).getData().getValue());

		assertEquals("Shyam", sheet.getRow(1).getColumn(0).getData().getValue());
		assertEquals("20", sheet.getRow(1).getColumn(1).getData().getValue());
	}

	@Test
	public void test_col_withoutHeader_rowwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITHOUT_HEADER, false, false);

		assertNull(sheet.getHeader());

		assertEquals("Ram", sheet.getColumns(0).get(0).getValue());
		assertEquals("10", sheet.getColumns(1).get(0).getValue());

		assertEquals("Shyam", sheet.getColumns(0).get(1).getValue());
		assertEquals("20", sheet.getColumns(1).get(1).getValue());
	}

	@Test
	public void test_col_withoutHeader_colwise() throws FileReaderException {
		SheetDetails sheet = excelFileReader.readSheet(SHEET_WITHOUT_HEADER, false, true);

		assertNull(sheet.getHeader());

		assertEquals("Ram", sheet.getColumns(0).get(0).getValue());
		assertEquals("10", sheet.getColumns(1).get(0).getValue());

		assertEquals("Shyam", sheet.getColumns(0).get(1).getValue());
		assertEquals("20", sheet.getColumns(1).get(1).getValue());
	}

	@Test
	public void test_model_withHeader_rowwise() throws FileReaderException, InvalidModelException {
		List<PersonWithHeaderRowwise> persons = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME,
				PersonWithHeaderRowwise.class);
		Assert.assertEquals(2, persons.size());
	}

	@Test
	public void test_model_withoutHeader_rowwise() throws FileReaderException, InvalidModelException {
		List<PersonWithoutHeaderRowwise> persons = excelFileReader.readSheet(SHEET_WITHOUT_HEADER,
				PersonWithoutHeaderRowwise.class);
		Assert.assertEquals(2, persons.size());
	}

	@Test
	public void test_model_withHeader_colwise() throws FileReaderException, InvalidModelException {
		List<PersonWithHeaderColwise> persons = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME,
				PersonWithHeaderColwise.class);
		Assert.assertEquals(1, persons.size());
		Assert.assertEquals(2, persons.get(0).getNames().size());
	}

	@Test
	public void test_model_withoutHeader_colwise() throws FileReaderException, InvalidModelException {
		List<PersonWithoutHeaderColwise> persons = excelFileReader.readSheet(SHEET_WITHOUT_HEADER,
				PersonWithoutHeaderColwise.class);
		Assert.assertEquals(1, persons.size());
		Assert.assertEquals(2, persons.get(0).getNames().size());
	}

	public void test_model_without_classType_annotation() throws FileReaderException, InvalidModelException {
		List<PersonWithoutClassTypeAnnotation> persons = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_DIFF_NAME,
				PersonWithoutClassTypeAnnotation.class);
		Assert.assertEquals(2, persons.size());
		Assert.assertEquals("Ram", persons.get(0).getName());
		Assert.assertEquals(10, persons.get(0).getAge());
	}

	@Test
	public void test_model_without_FieldType_annotation() throws FileReaderException, InvalidModelException {
		List<PersonWithoutFieldTypeAnnotation> persons = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_SAME_NAME,
				PersonWithoutFieldTypeAnnotation.class);
		Assert.assertEquals(2, persons.size());
		Assert.assertEquals("Ram", persons.get(0).getName());
		Assert.assertEquals(10, persons.get(0).getAge());
	}

	@Test
	public void test_model_without_any_annotation() throws FileReaderException, InvalidModelException {
		List<PersonWithoutAnyAnnotation> persons = excelFileReader.readSheet(SHEET_WITH_HEADER_WITH_SAME_NAME,
				PersonWithoutAnyAnnotation.class);
		Assert.assertEquals(2, persons.size());
		Assert.assertEquals("Ram", persons.get(0).getName());
		Assert.assertEquals(10, persons.get(0).getAge());
	}

	@Test
	@SuppressWarnings("unchecked")
	public void test_model_withHeader_rowwise_workbook() throws FileReaderException, InvalidModelException {
		Map<String, Class<?>> map = new HashMap<>();
		map.put(SHEET_WITH_HEADER_WITH_DIFF_NAME, PersonWithHeaderRowwise.class);
		map.put(SHEET_WITH_HEADER_WITH_DIFF_NAME_2, PersonWithHeaderColwise.class);

		Map<String, List<?>> result = excelFileReader.readWorkbook(map);

		List<PersonWithHeaderRowwise> ls1 = (List<PersonWithHeaderRowwise>) result.get(SHEET_WITH_HEADER_WITH_DIFF_NAME);
		List<PersonWithHeaderColwise> ls2 = (List<PersonWithHeaderColwise>) result.get(SHEET_WITH_HEADER_WITH_DIFF_NAME_2);

		Assert.assertEquals(2, ls1.size());
		Assert.assertEquals(1, ls2.size());

		Assert.assertEquals("Ram", ls1.get(0).getName());
		Assert.assertEquals(10, ls1.get(0).getAge());

		Assert.assertEquals(2, ls2.get(0).getNames().size());
	}

	@After
	public void cleanup() {
		if (excelFile.exists()) {
			excelFile.delete();
		}
	}

}
