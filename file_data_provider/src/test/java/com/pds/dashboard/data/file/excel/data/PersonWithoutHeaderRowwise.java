package com.pds.dashboard.data.file.excel.data;

import com.pds.dashboard.data.file.excel.annotations.ExcelDataElement;
import com.pds.dashboard.data.file.excel.annotations.ExcelDataModel;

@ExcelDataModel(columnWise = false, withHeader = false)
public class PersonWithoutHeaderRowwise {
	@ExcelDataElement(index = 0)
	private String name;

	@ExcelDataElement(index = 1)
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}
}
