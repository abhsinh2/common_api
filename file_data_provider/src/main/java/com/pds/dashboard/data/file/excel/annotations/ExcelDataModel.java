package com.pds.dashboard.data.file.excel.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface ExcelDataModel {
	boolean withHeader();
	boolean columnWise() default false;
}
