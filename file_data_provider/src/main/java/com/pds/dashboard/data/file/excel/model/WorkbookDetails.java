package com.pds.dashboard.data.file.excel.model;

import java.util.HashMap;
import java.util.Map;

public class WorkbookDetails {
	private Map<String, SheetDetails> details = new HashMap<>();
	
	public void addSheet(String sheetname, SheetDetails sheetDetails) {
		details.put(sheetname, sheetDetails);
	}
	
	public SheetDetails getSheet(String sheetname) {
		return this.details.get(sheetname);
	}
	
	public Map<String, SheetDetails> getDetails() {
		return details;
	}

	@Override
	public String toString() {
		return "WorkbookDetails [details=" + details + "]";
	}
}
