package com.pds.dashboard.data.file.excel.model;

public class SheetColumnData {
	private String value;

	public SheetColumnData(String data) {
		this.value = data;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "SheetColumnData [value=" + value + "]";
	}
}
