package com.pds.dashboard.data.file.excel.model;

import java.util.ArrayList;
import java.util.List;

public class SheetRow {
	private List<SheetColumn> columns;

	public SheetRow() {
		columns = new ArrayList<>();
	}

	public void add(SheetColumn column) {
		this.columns.add(column);
	}

	public List<SheetColumn> getColumns() {
		return this.columns;
	}
	
	public SheetColumn getColumn(int index) {
		return this.columns.get(index);
	}

	@Override
	public String toString() {
		return "SheetRow [columns=" + columns + "]";
	}
}
