package com.pds.dashboard.data.file.excel.model;

import java.util.ArrayList;
import java.util.List;

public class RowWiseSheetDetails extends SheetDetails {
	private List<SheetRow> rows;

	public RowWiseSheetDetails(boolean withHeader) {
		super(withHeader);
		this.rows = new ArrayList<>();
	}

	@Override
	public void add(SheetRow row) {
		if (this.isWithHeader() && this.getHeader() == null) {
			this.addHeader(row);
			return;
		}
		this.rows.add(row);
	}

	@Override
	public SheetRow getRow(int index) {
		return this.rows.get(index);
	}

	@Override
	public List<SheetRow> getRows() {
		return this.rows;
	}

	@Override
	public List<SheetColumnData> getColumns(String key) {
		List<SheetColumnData> list = new ArrayList<>();
		this.getRows().forEach(row -> {
			row.getColumns().stream().filter(col -> {
				return col.getHeader().equals(key);
			}).forEach(col -> {
				list.add(col.getData());
			});
		});
		return list;
	}

	@Override
	public List<SheetColumnData> getColumns(int index) {
		List<SheetColumnData> list = new ArrayList<>();
		this.getRows().forEach(row -> {
			list.add(row.getColumn(index).getData());
		});
		return list;
	}

	@Override
	public String toString() {
		return "[rows=" + rows + ", header=" + getHeader() + ", isWithHeader()=" + isWithHeader() + "]";
	}

}
