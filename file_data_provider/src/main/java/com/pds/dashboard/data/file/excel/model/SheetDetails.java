package com.pds.dashboard.data.file.excel.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class SheetDetails {
	private SheetRow header;
	@JsonIgnore
	private boolean withHeader;

	public SheetDetails(boolean withHeader) {
		this.withHeader = withHeader;
	}

	public void addHeader(SheetRow row) {
		if (this.header == null) {
			this.header = row;
		}
	}

	public abstract void add(SheetRow row);

	public abstract List<SheetRow> getRows();

	public abstract SheetRow getRow(int index);

	public abstract List<SheetColumnData> getColumns(String key);

	public abstract List<SheetColumnData> getColumns(int index);

	public SheetRow getHeader() {
		return header;
	}

	public boolean isWithHeader() {
		return withHeader;
	}
}
