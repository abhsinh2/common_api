package com.pds.dashboard.data.file.excel.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ColumnWiseSheetDetails extends SheetDetails {
	private Map<String, List<SheetColumnData>> columns;

	public ColumnWiseSheetDetails(boolean withHeader) {
		super(withHeader);
		columns = new LinkedHashMap<>();
	}

	@Override
	public void add(SheetRow row) {
		if (this.isWithHeader() && this.getHeader() == null) {
			this.addHeader(row);
			return;
		}

		if (this.isWithHeader()) {
			row.getColumns().forEach(col -> {
				List<SheetColumnData> cols = this.columns.get(col.getHeader());
				if (cols == null) {
					cols = new ArrayList<>();
				}
				cols.add(col.getData());
				columns.put(col.getHeader(), cols);
			});
		} else {
			int index = 0;
			for (SheetColumn col : row.getColumns()) {
				List<SheetColumnData> cols = this.columns.get(String.valueOf(index));
				if (cols == null) {
					cols = new ArrayList<>();
				}
				cols.add(col.getData());
				columns.put(String.valueOf(index), cols);
				index++;
			}
		}
	}

	@Override
	public List<SheetColumnData> getColumns(String key) {
		return columns.get(key);
	}

	@Override
	public List<SheetColumnData> getColumns(int index) {
		return columns.get(String.valueOf(index));
	}

	@Override
	public List<SheetRow> getRows() {
		List<SheetRow> rows = new ArrayList<>();
		for (int i = 0; i < this.columns.size(); i++) {
			rows.add(this.getRow(i));
		}
		return rows;
	}

	@Override
	public SheetRow getRow(int index) {
		SheetRow row = new SheetRow();
		this.columns.entrySet().forEach(entry -> {
			List<SheetColumnData> columns = entry.getValue();
			SheetColumnData data = columns.get(index);
			SheetColumn col = new SheetColumn();
			col.setData(data);
			row.add(col);
		});
		return row;
	}

}
