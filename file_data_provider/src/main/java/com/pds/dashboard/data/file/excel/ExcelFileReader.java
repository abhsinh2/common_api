package com.pds.dashboard.data.file.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.pds.dashboard.data.file.FileReaderException;
import com.pds.dashboard.data.file.InvalidModelException;
import com.pds.dashboard.data.file.excel.model.SheetColumn;
import com.pds.dashboard.data.file.excel.model.SheetColumnData;
import com.pds.dashboard.data.file.excel.annotations.ExcelDataElement;
import com.pds.dashboard.data.file.excel.annotations.ExcelDataModel;
import com.pds.dashboard.data.file.excel.model.ColumnWiseSheetDetails;
import com.pds.dashboard.data.file.excel.model.SheetDetails;
import com.pds.dashboard.data.file.excel.model.SheetRow;
import com.pds.dashboard.data.file.excel.model.RowWiseSheetDetails;
import com.pds.dashboard.data.file.excel.model.WorkbookDetails;

public class ExcelFileReader {
	private File file;
	private Workbook workbook;
	private DataFormatter dataFormatter;

	public ExcelFileReader(File file) {
		this.file = file;
		dataFormatter = new DataFormatter();
	}

	private void openWorkbook() throws IOException {
		workbook = new XSSFWorkbook(new FileInputStream(this.file));
	}

	public WorkbookDetails readWorkbook(boolean withHeader, boolean columnWise) throws FileReaderException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		WorkbookDetails workbookDetails = new WorkbookDetails();
		workbook.forEach(sheet -> {
			workbookDetails.addSheet(sheet.getSheetName(), this.readSheet(sheet, withHeader, columnWise));
		});
		closeWorkbook();
		return workbookDetails;
	}

	public Map<String, List<?>> readWorkbook(Map<String, Class<?>> classMap)
			throws FileReaderException, InvalidModelException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		Map<String, List<?>> objMap = new HashMap<>();

		Iterator<Sheet> sheetIterator = workbook.iterator();
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			Class<?> tClass = classMap.get(sheet.getSheetName());
			if (tClass != null) {
				List<?> tObj = this.readSheet(sheet, tClass);
				objMap.put(sheet.getSheetName(), tObj);
			}
		}

		closeWorkbook();
		return objMap;
	}

	public SheetDetails readSheet(String sheetName, boolean withHeader, boolean columnWise) throws FileReaderException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		Sheet sheet = workbook.getSheet(sheetName);

		if (sheet == null) {
			throw new FileReaderException("Invalid Sheet Name " + sheetName);
		}

		SheetDetails sheetDetails = readSheet(sheet, withHeader, columnWise);
		closeWorkbook();
		return sheetDetails;
	}

	public <T> List<T> readSheet(String sheetName, Class<T> modelClass)
			throws FileReaderException, InvalidModelException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		Sheet sheet = workbook.getSheet(sheetName);

		if (sheet == null) {
			throw new FileReaderException("Invalid Sheet Name " + sheetName);
		}

		List<T> t = readSheet(sheet, modelClass);
		closeWorkbook();
		return t;
	}

	public SheetDetails readSheet(int sheetIndex, boolean withHeader, boolean columnWise) throws FileReaderException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		Sheet sheet = workbook.getSheetAt(sheetIndex);

		if (sheet == null) {
			throw new FileReaderException("Invalid Sheet index " + sheetIndex);
		}

		SheetDetails sheetDetails = readSheet(sheet, withHeader, columnWise);
		closeWorkbook();
		return sheetDetails;
	}

	public <T> List<T> readSheet(int sheetIndex, Class<T> modelClass)
			throws FileReaderException, InvalidModelException {
		try {
			openWorkbook();
		} catch (IOException e) {
			throw new FileReaderException("Error is opening workbook " + this.file.getPath());
		}

		Sheet sheet = workbook.getSheetAt(sheetIndex);

		if (sheet == null) {
			throw new FileReaderException("Invalid Sheet index " + sheetIndex);
		}

		List<T> t = readSheet(sheet, modelClass);
		closeWorkbook();
		return t;
	}

	private SheetDetails readSheet(Sheet sheet, boolean withHeader, boolean columnWise) {
		final SheetDetails sheetDetails;
		if (columnWise)
			sheetDetails = new ColumnWiseSheetDetails(withHeader);
		else
			sheetDetails = new RowWiseSheetDetails(withHeader);

		sheet.forEach(row -> {
			if (withHeader) {
				if (sheetDetails.getHeader() == null) {
					sheetDetails.addHeader(readRow(row, null));
				} else {
					sheetDetails.add(readRow(row, sheetDetails.getHeader()));
				}
			} else {
				sheetDetails.add(readRow(row, null));
			}
		});

		return sheetDetails;
	}

	private <T> List<T> readSheet(Sheet sheet, Class<T> modelClass) throws InvalidModelException {
		List<T> list = new ArrayList<>();

		try {
			T tObj = modelClass.newInstance();
			ExcelDataModel excelDataModel = tObj.getClass().getAnnotation(ExcelDataModel.class);

			boolean columnWise = false;
			boolean withHeader = true;
			
			if (excelDataModel != null) {
				columnWise = excelDataModel.columnWise();
				withHeader = excelDataModel.withHeader();
			}			

			Iterator<Row> rowIterator = sheet.iterator();

			Row header = null;
			if (withHeader) {
				header = rowIterator.next();
			}

			if (columnWise) {
				list.add(this.readDataByColumn(rowIterator, header, modelClass));
			} else {
				while (rowIterator.hasNext()) {
					Row currentRow = rowIterator.next();
					list.add(this.readDataByRow(currentRow, header, modelClass));
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}

		return list;
	}

	private <T> T readDataByRow(Row currentRow, Row header, Class<T> modelClass) {
		T tObj = null;
		try {
			tObj = modelClass.newInstance();

			for (Field field : tObj.getClass().getDeclaredFields()) {
				ExcelDataElement excelDataElement = field.getAnnotation(ExcelDataElement.class);
				if (excelDataElement != null) {
					String excelColumn = excelDataElement.column();

					Cell currentCell = null;
					if (StringUtils.isEmpty(excelColumn)) {
						int excelColumnIndex = excelDataElement.index();
						currentCell = currentRow.getCell(excelColumnIndex);
					} else {
						currentCell = this.getCell(excelColumn, currentRow, header);
					}

					if (currentCell != null) {
						Object currentCellValue = this.getCellValue(currentCell);
						BeanUtils.setProperty(tObj, field.getName(), currentCellValue);
					}
				} else {
					String excelColumn = field.getName();
					Cell currentCell = this.getCell(excelColumn, currentRow, header);

					if (currentCell != null) {
						Object currentCellValue = this.getCellValue(currentCell);
						BeanUtils.setProperty(tObj, field.getName(), currentCellValue);
					}
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		return tObj;
	}
	
	

	private <T> T readDataByColumn(Iterator<Row> rowIterator, Row header, Class<T> modelClass) {
		Map<Integer, List<Object>> map = new HashMap<>();

		while (rowIterator.hasNext()) {
			Row currentRow = rowIterator.next();
			Iterator<Cell> cellIterator = currentRow.cellIterator();

			int i = 0;
			while (cellIterator.hasNext()) {
				List<Object> list = map.get(i);
				if (list == null) {
					list = new ArrayList<>();
					map.put(i, list);
				}

				Cell currentCell = cellIterator.next();
				list.add(this.getCellValue(currentCell));
				i++;
			}
		}

		T tObj = null;
		try {
			tObj = modelClass.newInstance();

			for (Field field : tObj.getClass().getDeclaredFields()) {
				ExcelDataElement excelDataElement = field.getAnnotation(ExcelDataElement.class);
				if (excelDataElement != null) {
					String excelColumn = excelDataElement.column();

					List<Object> values = null;
					if (StringUtils.isEmpty(excelColumn)) {
						int excelColumnIndex = excelDataElement.index();
						values = map.get(excelColumnIndex);
					} else {
						values = map.get(this.getCell(excelColumn, header));
					}

					if (values != null) {
						BeanUtils.setProperty(tObj, field.getName(), values);
					}
				}
			}
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return tObj;
	}

	private Cell getCell(String excelColumn, Row currentRow, Row header) {
		Iterator<Cell> currentCellIterator = currentRow.iterator();
		Iterator<Cell> headerCellIterator = header.iterator();

		while (currentCellIterator.hasNext() && headerCellIterator.hasNext()) {
			Cell currentCell = currentCellIterator.next();
			Cell headerCell = headerCellIterator.next();

			String headerCellValue = this.getCellValueAsString(headerCell);
			if (headerCellValue.equals(excelColumn.trim())) {
				return currentCell;
			}
		}
		return null;
	}

	private int getCell(String excelColumn, Row header) {
		Iterator<Cell> headerCellIterator = header.iterator();

		int i = 0;
		while (headerCellIterator.hasNext()) {
			Cell headerCell = headerCellIterator.next();

			String headerCellValue = this.getCellValueAsString(headerCell);
			if (headerCellValue.equals(excelColumn.trim())) {
				return i;
			}
			i++;
		}
		return -1;
	}

	private SheetRow readRow(Row row, SheetRow header) {
		SheetRow rowDetails = new SheetRow();
		Integer index = 0;
		for (Cell cell : row) {
			String cellValue = this.getCellValueAsString(cell);

			SheetColumn sheetColumn = new SheetColumn();
			if (header != null) {
				sheetColumn.setHeader(header.getColumns().get(index).getData().getValue());
			}
			sheetColumn.setData(new SheetColumnData(cellValue));

			rowDetails.add(sheetColumn);
			index++;
		}
		return rowDetails;
	}

	private Object getCellValue(Cell currentCell) {
		switch (currentCell.getCellTypeEnum()) {
		case BOOLEAN:
			return Boolean.valueOf(currentCell.getBooleanCellValue());
		case STRING:
			return currentCell.getRichStringCellValue().getString();
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(currentCell)) {
				return currentCell.getDateCellValue();
			} else {
				return currentCell.getNumericCellValue();
			}
		case BLANK:
			return "";
		default:
			return dataFormatter.formatCellValue(currentCell).trim();
		}
	}

	private String getCellValueAsString(Cell currentCell) {
		return dataFormatter.formatCellValue(currentCell).trim();
	}

	private void closeWorkbook() {
		try {
			this.workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
