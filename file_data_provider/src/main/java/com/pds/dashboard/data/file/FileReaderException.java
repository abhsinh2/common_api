package com.pds.dashboard.data.file;

public class FileReaderException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public FileReaderException(String message) {
		super(message);
	}
}
