package com.pds.dashboard.data.file;

import java.io.File;
import java.util.List;
import java.util.Map;

import com.pds.dashboard.data.DataProvider;

public interface FileDataProvider extends DataProvider {
	public Map<Integer, List<String>> read(File file) throws FileReaderException;
}
