package com.pds.dashboard.data.file.excel.model;

public class SheetColumn {
	private String header;
	private SheetColumnData data;

	public SheetColumn() {
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public SheetColumnData getData() {
		return data;
	}

	public void setData(SheetColumnData data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "SheetColumn [header=" + header + ", column=" + data + "]";
	}
}
