package com.abhsinh2.log.manager.service;

import org.springframework.stereotype.Component;

import com.abhsinh2.log.manager.api.LogListener;
import com.abhsinh2.log.manager.api.internal.LogType;
import com.abhsinh2.log.manager.api.matcher.Matcher;
import com.abhsinh2.log.manager.api.matcher.MatcherBuilder;

@Component
public class ErrorLogListener implements LogListener {

	@Override
	public Matcher applyMatcher() {
		return MatcherBuilder.createBuilder().forType(new LogType[] { LogType.ERROR }).build();
	}

	@Override
	public void listen(String message) {
		System.out.println(message);
	}

}
