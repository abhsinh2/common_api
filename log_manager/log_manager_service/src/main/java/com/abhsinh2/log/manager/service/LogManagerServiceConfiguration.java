package com.abhsinh2.log.manager.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.abhsinh2.log.manager.api"})
public class LogManagerServiceConfiguration {

}
