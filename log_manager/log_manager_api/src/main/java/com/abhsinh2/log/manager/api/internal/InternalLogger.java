package com.abhsinh2.log.manager.api.internal;

import org.slf4j.Logger;
import org.slf4j.helpers.MessageFormatter;

import com.abhsinh2.log.manager.api.ILogger;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.NullNode;

public class InternalLogger implements ILogger {
	private static final String MESSAGE = "message";
	private static final String TYPE = "type";

	private Logger logger;
	private LogEventPublisher processor;
	private JsonNode logMessageJson;
	private Class<?> clazz;

	public InternalLogger(Logger logger, Class<?> clazz, LogEventPublisher processor, JsonNode logMessageJson) {
		this.logger = logger;
		this.processor = processor;
		this.logMessageJson = logMessageJson;
		this.clazz = clazz;
	}

	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	public void debug(String msg) {
		_log(LogType.DEBUG, Boolean.TRUE, msg);
	}

	public void debug(String msg, Object... args) {
		_log(LogType.DEBUG, Boolean.TRUE, msg, args);
	}

	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	public void info(String msg) {
		_log(LogType.INFO, Boolean.TRUE, msg);
	}

	public void info(String msg, Object... args) {
		_log(LogType.INFO, Boolean.TRUE, msg, args);
	}

	public boolean isWarnEnabled() {
		return logger.isWarnEnabled();
	}

	public void warn(String msg) {
		_log(LogType.WARN, Boolean.TRUE, msg);
	}

	public void warn(String msg, Object... args) {
		_log(LogType.WARN, Boolean.TRUE, msg, args);
	}

	public boolean isErrorEnabled() {
		return logger.isErrorEnabled();
	}

	public void error(String msg) {
		_log(LogType.ERROR, Boolean.TRUE, msg);
	}

	public void error(String msg, Object... args) {
		_log(LogType.ERROR, Boolean.TRUE, msg, args);
	}

	public void error(String msg, Throwable t) {
		logger.error(msg, t);
	}

	private void _log(LogType logType, boolean toPublish, String message) {
		if (logType == LogType.INFO) {
			logger.info(message);
		} else if (logType == LogType.DEBUG) {
			logger.debug(message);
		} else if (logType == LogType.WARN) {
			logger.warn(message);
		} else if (logType == LogType.ERROR) {
			logger.error(message);
		}

		if (toPublish) {
			publish(logType, message);
		}
	}

	private void _log(LogType logType, boolean toPublish, String message, Object... args) {
		if (logType == LogType.INFO) {
			logger.info(message, args);
		} else if (logType == LogType.DEBUG) {
			logger.debug(message, args);
		} else if (logType == LogType.WARN) {
			logger.warn(message, args);
		} else if (logType == LogType.ERROR) {
			logger.error(message, args);
		}

		if (toPublish) {
			publish(logType, this.format(message, args));
		}
	}

	public void log(String logCode) {
		JsonNode node = getNode(logCode);
		if (node != null && !(node instanceof NullNode)) {
			String message = node.get(MESSAGE).asText();
			String messageType = node.get(TYPE).asText();

			LogType logType = LogType.value(messageType);

			_log(logType, Boolean.FALSE, message);
			publish(logType, logCode, message);
		}
	}

	public void log(String logCode, Object... args) {
		JsonNode node = getNode(logCode);
		if (node != null && !(node instanceof NullNode)) {
			String message = node.get(MESSAGE).asText();
			String messageType = node.get(TYPE).asText();

			LogType logType = LogType.value(messageType);

			_log(logType, Boolean.FALSE, message, args);
			publish(logType, logCode, this.format(message, args), args);
		}
	}

	private void publish(LogType logType, String message) {
		if (processor != null)
			processor.publish(new Record(null, logType, this.clazz, message));
	}

	private void publish(LogType logType, String logCode, String message, Object... args) {
		if (processor != null)
			processor.publish(new Record(null, logType, this.clazz, message, logCode, args));
	}

	private JsonNode getNode(String logCode) {
		return this.logMessageJson.get(logCode);
	}

	public String format(String message, Object... objs) {
		return MessageFormatter.arrayFormat(message, objs).getMessage();
	}
}
