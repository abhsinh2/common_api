package com.abhsinh2.log.manager.api;

import com.abhsinh2.log.manager.api.matcher.Matcher;

public interface LogListener {
	public Matcher applyMatcher();

	public void listen(String message);
}
