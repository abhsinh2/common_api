package com.abhsinh2.log.manager.api.matcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import com.abhsinh2.log.manager.api.internal.LogType;
import com.abhsinh2.log.manager.api.internal.matcher.ClassMatcher;
import com.abhsinh2.log.manager.api.internal.matcher.ContentMatcher;
import com.abhsinh2.log.manager.api.internal.matcher.LogTypeMatcher;

public class MatcherBuilder {
	private Collection<MatcherBuilder> matcherBuilders = new ArrayList<>();
	private MatcherBuilderDetails matcherBuilderDetails = new MatcherBuilderDetails();

	private MatcherBuilder() {

	}

	public static MatcherBuilder createBuilder() {
		MatcherBuilder filterBuilder = new MatcherBuilder();
		return filterBuilder;
	}

	public MatcherBuilder forType(LogType... logTypes) {
		for (LogType logType : logTypes) {
			this.add(new LogTypeMatcher(logType));
		}
		return this;
	}

	public MatcherBuilder from(Class<?>... classes) {
		for (Class<?> clazz : classes) {
			this.add(new ClassMatcher(clazz));
		}
		return this;
	}

	public MatcherBuilder havingContent(String... contents) {
		for (String content : contents) {
			this.add(new ContentMatcher(content));
		}
		return this;
	}

	public Matcher build() {
		Collection<MatcherBuilderDetails> matcherBuilderDetails = new ArrayList<>(this.matcherBuilders.size() + 1);
		matcherBuilderDetails.add(this.matcherBuilderDetails);
		
		for (MatcherBuilder matcherBuilder : this.matcherBuilders) {
			matcherBuilderDetails.add(matcherBuilder.matcherBuilderDetails);
		}
		return new DefaultMatcher(matcherBuilderDetails);
	}

	public MatcherBuilder and() {
		MatcherBuilder matcherBuilder = new MatcherBuilder();
		matcherBuilders.add(matcherBuilder);
		return matcherBuilder;
	}

	private void add(LogTypeMatcher logTypeMatcher) {
		this.matcherBuilderDetails.getLogTypeMatchers().add(logTypeMatcher);
	}

	private void add(ClassMatcher classMatcher) {
		this.matcherBuilderDetails.getClassMatchers().add(classMatcher);
	}

	private void add(ContentMatcher contentMatcher) {
		this.matcherBuilderDetails.getContentMatchers().add(contentMatcher);
	}

	static class MatcherBuilderDetails {
		private Collection<LogTypeMatcher> logTypeMatchers = new HashSet<>();
		private Collection<ClassMatcher> classMatchers = new HashSet<>();
		private Collection<ContentMatcher> contentMatchers = new HashSet<>();

		MatcherBuilderDetails() {
		}

		public Collection<LogTypeMatcher> getLogTypeMatchers() {
			return logTypeMatchers;
		}

		public Collection<ClassMatcher> getClassMatchers() {
			return classMatchers;
		}

		public Collection<ContentMatcher> getContentMatchers() {
			return contentMatchers;
		}
	}
}
