package com.abhsinh2.log.manager.api;

import com.abhsinh2.log.manager.api.internal.InternalLogger;

public class Logger implements ILogger {

	private InternalLogger logger;

	Logger(InternalLogger logger) {
		this.logger = logger;
	}

	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}

	public void debug(String msg) {
		logger.debug(msg);
	}

	public void debug(String msg, Object... arguments) {
		logger.debug(msg, arguments);
	}

	public boolean isInfoEnabled() {
		return logger.isInfoEnabled();
	}

	public void info(String message) {
		this.logger.info(message);
	}

	public void info(String msg, Object... arguments) {
		logger.info(msg, arguments);
	}

	public boolean isWarnEnabled() {
		return logger.isWarnEnabled();
	}

	public void warn(String msg) {
		logger.warn(msg);
	}

	public void warn(String msg, Object... arguments) {
		logger.warn(msg, arguments);
	}

	public boolean isErrorEnabled() {
		return logger.isErrorEnabled();
	}

	public void error(String msg) {
		logger.error(msg);
	}

	public void error(String msg, Object... arguments) {
		logger.error(msg, arguments);
	}

	public void error(String msg, Throwable t) {
		logger.error(msg, t);
	}
	
	public void log(String logCode) {
		logger.log(logCode);
	}
	
	public void log(String logCode, Object... args) {
		logger.log(logCode, args);
	}

	public String format(String message, Object... objs) {
		return logger.format(message, objs);
	}
}
