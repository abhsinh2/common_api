package com.abhsinh2.log.manager.api;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.abhsinh2.log.manager.api.internal"})
public class LogManagerConfiguration {

}
