package com.abhsinh2.log.manager.api.internal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

import com.abhsinh2.log.manager.api.LogEvent;
import com.abhsinh2.log.manager.api.LogListener;
import com.abhsinh2.log.manager.api.matcher.Matcher;

@Component
public class LogEventPublisher implements ApplicationEventPublisherAware {
	@Autowired
	private List<LogListener> logListeners;
	
	private ApplicationEventPublisher applicationEventPublisher;
	
	public void publish(Record record) {
		if (logListeners != null) {
			logListeners.stream().forEach(logListener -> {
				Matcher matcher = logListener.applyMatcher();
				if (matcher.matches(record)) {
					logListener.listen(record.getMessage());
				}
			});
		}
		
		this.publish(new LogEvent(record.getClazz(), record));
	}

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		this.applicationEventPublisher = applicationEventPublisher;
	}
	
	public void publish(LogEvent event) {
		applicationEventPublisher.publishEvent(event);
	}
}
