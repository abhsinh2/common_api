package com.abhsinh2.log.manager.api;

import com.abhsinh2.log.manager.api.internal.InternalLogFactory;
import com.abhsinh2.log.manager.api.internal.InternalLogger;

public class LogFactory {	
	public static Logger createLogger(Class<?> clazz) {
		InternalLogger internalLogger = InternalLogFactory.createLogger(clazz);
		return new Logger(internalLogger);
	}
}
