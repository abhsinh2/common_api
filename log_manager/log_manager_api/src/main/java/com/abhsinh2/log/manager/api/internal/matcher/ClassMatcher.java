package com.abhsinh2.log.manager.api.internal.matcher;

import com.abhsinh2.log.manager.api.internal.Record;
import com.abhsinh2.log.manager.api.matcher.Matcher;

public class ClassMatcher implements Matcher {

	private Class<?> clazz;

	public ClassMatcher(Class<?> clazz) {
		this.clazz = clazz;
	}

	@Override
	public boolean matches(Record record) {
		return record.getClazz() == this.clazz;
	}

}
