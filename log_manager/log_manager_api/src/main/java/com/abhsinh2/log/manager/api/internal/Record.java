package com.abhsinh2.log.manager.api.internal;

import java.io.Serializable;
import java.util.Date;

public class Record implements Serializable {
	private static final long serialVersionUID = 8154047772191796444L;
	
	private Date date;
	private LogType logType;
	private Class<?> clazz;
	private String message;

	private String code;
	private Object[] args;

	public Record(Date date, LogType logType, Class<?> clazz, String message) {
		super();
		this.date = date;
		this.logType = logType;
		this.clazz = clazz;
		this.message = message;
	}

	public Record(Date date, LogType logType, Class<?> clazz, String message, String code, Object[] args) {
		super();
		this.date = date;
		this.logType = logType;
		this.clazz = clazz;
		this.message = message;
		this.code = code;
		this.args = args;
	}

	public Date getDate() {
		return date;
	}

	public LogType getLogType() {
		return logType;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}

	public Object[] getArgs() {
		return args;
	}
}
