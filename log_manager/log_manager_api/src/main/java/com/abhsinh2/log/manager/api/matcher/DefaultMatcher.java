package com.abhsinh2.log.manager.api.matcher;

import java.util.Collection;

import com.abhsinh2.log.manager.api.internal.Record;
import com.abhsinh2.log.manager.api.internal.matcher.ClassMatcher;
import com.abhsinh2.log.manager.api.internal.matcher.ContentMatcher;
import com.abhsinh2.log.manager.api.internal.matcher.LogTypeMatcher;

public class DefaultMatcher implements Matcher {

	private Collection<MatcherBuilder.MatcherBuilderDetails> matcherBuilderDetails;

	DefaultMatcher(Collection<MatcherBuilder.MatcherBuilderDetails> matcherBuilderDetails) {
		this.matcherBuilderDetails = matcherBuilderDetails;
	}

	@Override
	public boolean matches(Record record) {
		for (MatcherBuilder.MatcherBuilderDetails builderDetails : this.matcherBuilderDetails) {
			for (LogTypeMatcher logTypeMatcher : builderDetails.getLogTypeMatchers()) {
				if (!logTypeMatcher.matches(record)) {
					return false;
				}
			}

			for (ClassMatcher classMatcher : builderDetails.getClassMatchers()) {
				if (!classMatcher.matches(record)) {
					return false;
				}
			}

			for (ContentMatcher contentMatcher : builderDetails.getContentMatchers()) {
				if (!contentMatcher.matches(record)) {
					return false;
				}
			}
		}
		return true;
	}

}
