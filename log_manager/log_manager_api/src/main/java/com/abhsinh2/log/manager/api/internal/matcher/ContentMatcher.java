package com.abhsinh2.log.manager.api.internal.matcher;

import com.abhsinh2.log.manager.api.internal.Record;
import com.abhsinh2.log.manager.api.matcher.Matcher;

public class ContentMatcher implements Matcher {
	private String content;

	public ContentMatcher(String content) {
		this.content = content;
	}

	@Override
	public boolean matches(Record record) {
		return record.getMessage().contains(content);
	}
}
