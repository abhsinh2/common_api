package com.abhsinh2.log.manager.api.internal.matcher;

import com.abhsinh2.log.manager.api.internal.LogType;
import com.abhsinh2.log.manager.api.internal.Record;
import com.abhsinh2.log.manager.api.matcher.Matcher;

public class LogTypeMatcher implements Matcher {	
	
	private LogType logType;

	public LogTypeMatcher(LogType logType) {
		this.logType = logType;
	}

	@Override
	public boolean matches(Record record) {
		return record.getLogType() == this.logType;
	}

}
