package com.abhsinh2.log.manager.api.internal;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InternalLogFactory {
	private static LogEventPublisher dummyLogListenerProcessor;
	private static AnnotationScanner dummyAnnotationScanner;

	@Autowired
	private LogEventPublisher logListenerProcessor;

	@Autowired
	private AnnotationScanner annotationScanner;

	@PostConstruct
	private void init() {
		InternalLogFactory.dummyLogListenerProcessor = this.logListenerProcessor;
		InternalLogFactory.dummyAnnotationScanner = this.annotationScanner;
	}

	public static InternalLogger createLogger(Class<?> clazz) {
		Logger slf4jLogger = LoggerFactory.getLogger(clazz);
		return new InternalLogger(slf4jLogger, clazz, dummyLogListenerProcessor,
				dummyAnnotationScanner.getLogMessages());
	}
}
