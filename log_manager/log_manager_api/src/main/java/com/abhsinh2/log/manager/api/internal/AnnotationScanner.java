package com.abhsinh2.log.manager.api.internal;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

import com.abhsinh2.log.manager.api.annotation.EnableLogManager;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

@Configuration
public class AnnotationScanner implements ApplicationContextAware {
	
	private JsonNode logMessages;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		Map<String,Object> beans = applicationContext.getBeansWithAnnotation(EnableLogManager.class);
		
		beans.forEach((str, obj) -> {
			EnableLogManager st = applicationContext.findAnnotationOnBean(str, EnableLogManager.class);
			if (st != null) {
				Arrays.asList(st.filename()).forEach(fname -> {
					try {
						File file = ResourceUtils.getFile(fname);
						ObjectMapper mapper = new ObjectMapper();
						JsonNode temp = mapper.readTree(file);
						
						if (logMessages == null) {
							logMessages = temp;
						} else {
							Iterator<Map.Entry<String, JsonNode>> iter = temp.fields();
							while (iter.hasNext()) {
								Map.Entry<String, JsonNode> entry = iter.next();
								((ObjectNode)logMessages).set(entry.getKey(), entry.getValue());
							}							
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (JsonProcessingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				});
			}
		});
	}

	public JsonNode getLogMessages() {
		return logMessages;
	}

}
