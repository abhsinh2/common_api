package com.abhsinh2.log.manager.api.annotation;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.abhsinh2.log.manager.api.internal.LogType;

@Retention(RUNTIME)
@Target(METHOD)
public @interface LogListener {
	LogType[] logTypes() default { LogType.ERROR };

	Class<?>[] components() default {};

	String[] contents() default {};
}
