package com.abhsinh2.log.manager.api;

import org.springframework.context.ApplicationEvent;

import com.abhsinh2.log.manager.api.internal.Record;

public class LogEvent extends ApplicationEvent {
	private static final long serialVersionUID = -9140078577866252324L;
	
	private Record record;
	
	public LogEvent(Object source, Record record) {
		super(source);
		this.record = record;
	}

	public Record getRecord() {
		return record;
	}	

}
