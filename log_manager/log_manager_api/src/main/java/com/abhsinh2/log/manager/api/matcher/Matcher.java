package com.abhsinh2.log.manager.api.matcher;

import com.abhsinh2.log.manager.api.internal.Record;

public interface Matcher {
	public boolean matches(Record record);
}
