package com.abhsinh2.log.manager.api.internal;

public enum LogType {
	ERROR("error"), INFO("info"), DEBUG("debug"), WARN("warn");

	private String type;

	private LogType(String type) {
		this.type = type;
	}

	public String getType() {
		return this.type;
	}
	
	public static LogType value(String value) {
		if (value.equals(ERROR.type)) {
			return ERROR;
		} else if (value.equals(INFO.type)) {
			return INFO;
		} else if (value.equals(DEBUG.type)) {
			return DEBUG;
		} else if (value.equals(WARN.type)) {
			return WARN;
		}
		throw new IllegalArgumentException("No enum constant " + value);
	}
}
