package com.abhsinh2.log.manager.api;

public interface ILogger {
	public boolean isDebugEnabled();

	public void debug(String msg);

	public void debug(String msg, Object... arguments);

	public boolean isInfoEnabled();

	public void info(String message);

	public void info(String msg, Object... arguments);

	public boolean isWarnEnabled();

	public void warn(String msg);

	public void warn(String msg, Object... arguments);

	public boolean isErrorEnabled();

	public void error(String msg);

	public void error(String msg, Object... arguments);

	public void error(String msg, Throwable t);
	
	public void log(String logCode);
	
	public void log(String logCode, Object... args);
	
	public String format(String message, Object... objs);
}
