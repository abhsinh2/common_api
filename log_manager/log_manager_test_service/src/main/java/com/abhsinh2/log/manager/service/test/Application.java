package com.abhsinh2.log.manager.service.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({ "com.abhsinh2.log.manager.api.internal", "com.abhsinh2.log.manager.api",
		"com.abhsinh2.log.manager.service", "com.abhsinh2.log.manager.service.test" })
public class Application {
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
