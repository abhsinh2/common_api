package com.abhsinh2.log.manager.service.test;

import org.springframework.context.annotation.Configuration;

import com.abhsinh2.log.manager.api.annotation.EnableLogManager;

@EnableLogManager
@Configuration
public class LogConfiguration {

}
