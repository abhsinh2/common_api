package com.abhsinh2.log.manager.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LogTestController {

	@Autowired
	private LogTestService logTestService;

	@RequestMapping("/log")
	public ResponseEntity<Void> log(@RequestParam(value = "type", defaultValue = "debug") String type) {
		if (type.equals("debug")) {
			logTestService.logDebugSomething();
		} else if (type.equals("info")) {
			logTestService.logInfoSomething();
		} else if (type.equals("error")) {
			logTestService.logErrorSomething();
		}
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
