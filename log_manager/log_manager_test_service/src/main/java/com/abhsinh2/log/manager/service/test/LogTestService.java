package com.abhsinh2.log.manager.service.test;

import org.springframework.stereotype.Service;

import com.abhsinh2.log.manager.api.Logger;
import com.abhsinh2.log.manager.api.LogFactory;

@Service
public class LogTestService {
	private static Logger logger = LogFactory.createLogger(LogTestService.class);
	
	public void logErrorSomething() {
		logger.error("Some error");
		logger.log("ERROR101");
		logger.log("ERROR102");
		logger.log("ERROR102", "hello");
		logger.log("ERROR103");
	}

	public void logDebugSomething() {
		logger.debug("Some debug");
	}

	public void logInfoSomething() {
		logger.info("Some info");
	}
}
