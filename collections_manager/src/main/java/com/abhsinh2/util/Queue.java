package com.abhsinh2.util;

import java.util.Collection;
import java.util.Iterator;

public class Queue<E> implements java.util.Queue<E> {
	private java.util.Queue<E> queue;
	
	public Queue(java.util.Queue<E> queue) {
		this.queue = queue;
	}

	public int size() {
		return queue.size();
	}

	public boolean isEmpty() {
		return this.queue.isEmpty();
	}

	public boolean contains(Object o) {
		return this.queue.contains(o);
	}

	public Iterator<E> iterator() {
		return this.queue.iterator();
	}

	public Object[] toArray() {
		return this.queue.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return this.queue.toArray(a);
	}

	public boolean remove(Object o) {
		return this.queue.remove(o);
	}

	public boolean containsAll(Collection<?> c) {
		return this.queue.containsAll(c);
	}

	public boolean addAll(Collection<? extends E> c) {
		return this.queue.addAll(c);
	}

	public boolean removeAll(Collection<?> c) {
		return this.queue.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return this.queue.retainAll(c);
	}

	public void clear() {
		this.queue.clear();
	}

	public boolean add(E e) {
		return this.queue.add(e);
	}

	public boolean offer(E e) {
		return this.queue.offer(e);
	}

	public E remove() {
		return this.queue.remove();
	}

	public E poll() {
		return this.queue.poll();
	}

	public E element() {
		return this.queue.element();
	}

	public E peek() {
		return this.queue.peek();
	}
}
