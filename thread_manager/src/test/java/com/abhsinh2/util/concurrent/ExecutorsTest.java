package com.abhsinh2.util.concurrent;

import java.util.concurrent.ExecutorService;

import org.junit.Test;

public class ExecutorsTest {
	@Test
	public void newFixedThreadPool() {
		ExecutorContext executorContext = new ExecutorContext();
		executorContext.setTenantId("executorContext");
		
		ExecutorService executorService = Executors.newFixedThreadPool(1, executorContext);
		executorService.execute(new Runnable() {			
			public void run() {
				System.out.println("Hello");
			}
		});
	}
}
