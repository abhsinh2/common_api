package com.abhsinh2.util.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import org.junit.Test;

public class CustomCallableTest {
	@Test
	public void test() {
		Callable<String> runSomething = new Callable<String>() {
			public String call() throws Exception {
				return "Hello";
			}
		};
		
		ThreadContext threadContext = new ThreadContext();
		threadContext.setTenantId("threadContext");
		
		CustomCallable<String> myCallable = new CustomCallable<String>(runSomething, threadContext);

		ExecutorContext executorContext = new ExecutorContext();
		executorContext.setTenantId("executorContext");
		
		ExecutorService executorService = Executors.newFixedThreadPool(1, executorContext);
		executorService.submit(myCallable);
	}
}
