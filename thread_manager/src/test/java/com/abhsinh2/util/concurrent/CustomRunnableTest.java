package com.abhsinh2.util.concurrent;

import java.util.concurrent.ExecutorService;

import org.junit.Test;

public class CustomRunnableTest {
	@Test
	public void test() {
		Runnable runSomething = new Runnable() {			
			public void run() {
				System.out.println("Running Something");
			}
		};
		
		ThreadContext threadContext = new ThreadContext();
		threadContext.setTenantId("threadContext");
		
		CustomRunnable myRunnable = new CustomRunnable(runSomething, threadContext);		
		
		ExecutorContext executorContext = new ExecutorContext();
		executorContext.setTenantId("executorContext");
		
		ExecutorService executorService = Executors.newFixedThreadPool(1, executorContext);
		executorService.execute(myRunnable);
	}
}
