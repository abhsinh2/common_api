package com.abhsinh2.util.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPoolExecutor extends java.util.concurrent.ThreadPoolExecutor {
	private ExecutorContext context;
	
	public ThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ExecutorContext context) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, Executors.defaultThreadFactory());
		this.context = context;
	}
	
	@Override
	public void execute(Runnable command) {
		System.out.println("Apply your logic " + context);
		super.execute(command);
	}
}
