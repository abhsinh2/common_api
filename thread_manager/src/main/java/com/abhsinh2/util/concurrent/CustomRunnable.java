package com.abhsinh2.util.concurrent;

public class CustomRunnable implements Runnable {
	
	private Runnable runnable;
	private ThreadContext context;
	
	public CustomRunnable(Runnable runnable, ThreadContext context) {
		this.runnable = runnable;
		this.context = context;
	}
	
	public void run() {
		System.out.println("Running for " + context);
		this.runnable.run();
	}

}
