package com.abhsinh2.util.concurrent;

public class ThreadContext {
	private String tenantId;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
	@Override
	public String toString() {
		return "[tenantId=" + tenantId + "]";
	}
}
