package com.abhsinh2.util.concurrent;

import java.util.concurrent.Callable;

public class CustomCallable<V> implements Callable<V> {
	
	private Callable<V> callable;
	private ThreadContext context;
	
	public CustomCallable(Callable<V> callable, ThreadContext context) {
		this.callable = callable;
		this.context = context;
	}

	public V call() throws Exception {
		System.out.println("Calling for " + this.context);
		return callable.call();
	}

}
