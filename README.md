# common_api

This repo will have common apis to be used accross many projects

### Log using log code or error code using external message file

See [Logging](./logging/README.md)

### Read Excel file and convert to your model class

See [File Data Provider](./file_data_provider/README.md)